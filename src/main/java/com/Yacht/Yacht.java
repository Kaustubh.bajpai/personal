package com.Yacht;

//Represents public domain dice game
class Yacht {
    private final String dicePips;
    private int score;
    private Catagory Combination;

    Yacht(String dicePips, Catagory Combination) {
        this.dicePips = dicePips;
        this.Combination = Combination;
    }

    int result() {
        if (dicePips.length() > 5) {
            return 0;
        }
        if (Combination.checkCatagory(dicePips) == true) {
            score = Combination.calculateScore(dicePips);
            return score;
        }
        return score;
    }
}
