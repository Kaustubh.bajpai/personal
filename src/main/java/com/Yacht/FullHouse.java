package com.Yacht;

import java.util.HashMap;
import java.util.Map;

public enum FullHouse implements Catagory {
    FULL_HOUSE;
    private int number;
    private int anotherNumber;

    @Override
    public int calculateScore(String pattern) {
        return (3 * number + 2 * anotherNumber);
    }

    @Override
    public boolean checkCatagory(String pattern) {
        boolean onePart = false;
        boolean secondPart = false;
        Map<Character, Integer> frequency = new HashMap<Character, Integer>();
        for (char ch : pattern.toCharArray()) {
            if (frequency.containsKey(ch)) {
                frequency.put(ch, frequency.get(ch) + 1);
            } else {
                frequency.put(ch, 1);
            }
        }
        for (Map.Entry<Character, Integer> entry : frequency.entrySet()) {
            if (entry.getValue() == 3) {
                number = Integer.parseInt(entry.getKey().toString());
                onePart = true;

            }
            if (entry.getValue() == 2) {
                anotherNumber = Integer.parseInt(entry.getKey().toString());
                secondPart = true;
            }

        }
        return onePart && secondPart;
    }
}
