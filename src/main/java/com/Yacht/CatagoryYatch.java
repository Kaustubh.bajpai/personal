package com.Yacht;

import java.util.HashMap;
import java.util.Map;

enum CatagoryYatch implements Catagory {
    Yatch();

    @Override
    public int calculateScore(String pattern) {
        return 50;
    }

    @Override
    public boolean checkCatagory(String pattern) {
        Map<Character, Integer> frequency = new HashMap<Character, Integer>();
        for (char ch : pattern.toCharArray()) {
            if (frequency.containsKey(ch)) {
                frequency.put(ch, frequency.get(ch) + 1);
            } else {
                frequency.put(ch, 1);
            }
        }
        for (Map.Entry<Character, Integer> entry : frequency.entrySet()) {
            if (entry.getValue() == 5) {
                return true;
            }
        }
        return false;
    }
}
