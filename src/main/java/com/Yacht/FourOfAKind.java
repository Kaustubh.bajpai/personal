package com.Yacht;

import java.util.HashMap;
import java.util.Map;

enum FourOfAKind implements Catagory {
    FOUR_OF_A_KIND();

    private Map<Character, Integer> frequency = new HashMap<Character, Integer>();
    private int factor;

    @Override
    public int calculateScore(String pattern) {
        return 4 * factor;
    }

    @Override
    public boolean checkCatagory(String pattern) {
        for (char ch : pattern.toCharArray()) {
            if (frequency.containsKey(ch)) {
                frequency.put(ch, frequency.get(ch) + 1);
            } else {
                frequency.put(ch, 1);
            }
        }
        for (Map.Entry<Character, Integer> entry : frequency.entrySet()) {
            if (entry.getValue() == 4) {
                factor = Integer.parseInt(entry.getKey().toString());
                return true;
            }
        }
        return false;
    }
}
