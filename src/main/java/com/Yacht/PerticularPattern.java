package com.Yacht;

enum PerticularPattern implements Catagory {
    LittleStraight("12345"),
    BigStraight("23456");

    private final String pattern;

    PerticularPattern(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public int calculateScore(String pattern) {
        return 30;
    }

    public boolean checkCatagory(String pattern) {
        return this.pattern == pattern;
    }

}
