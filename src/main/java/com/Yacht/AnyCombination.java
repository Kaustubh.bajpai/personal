package com.Yacht;

//defines catagory of the pips in Yatch game
enum AnyCombination implements Catagory {
    Ones(1),
    Twos(2),
    Threes(3),
    Fours(4),
    Fives(5),
    Sixes(6),
    Choice(0);
    private int factor;

    AnyCombination(int factor) {
        this.factor = factor;
    }

    public int calculateScore(String pips) {
        if (this.factor == 0) {
            return calculateScoreForChoice(pips);
        }
        int factorFrequency = 0;
        for (char ch : pips.toCharArray()) {
            if (Integer.parseInt(Character.toString(ch)) == factor) {
                factorFrequency++;
            }
        }
        return factor * factorFrequency;
    }

    @Override
    public boolean checkCatagory(String pattern) {
        return true;
    }

    private int calculateScoreForChoice(String pips) {
        int sumOfPips = 0;
        for (char ch : pips.toCharArray()) {
            sumOfPips += Integer.parseInt(Character.toString(ch));
        }
        return sumOfPips;
    }
}