package com.Yacht;

import org.junit.jupiter.api.Test;

import static com.Yacht.AnyCombination.*;
import static com.Yacht.CatagoryYatch.Yatch;
import static com.Yacht.FourOfAKind.FOUR_OF_A_KIND;
import static com.Yacht.FullHouse.FULL_HOUSE;
import static com.Yacht.PerticularPattern.BigStraight;
import static com.Yacht.PerticularPattern.LittleStraight;
import static org.junit.jupiter.api.Assertions.assertEquals;

class YachtTest {
    @Test
    void expectFiveWhenAllSidesAreOne() {
        Yacht game = new Yacht("11111", Ones);
        assertEquals(5, game.result());

    }

    @Test
    void expect10WhenAllSidesAreTwo() {
        Yacht game = new Yacht("22222", Twos);
        assertEquals(10, game.result());

    }

    @Test
    void expectZeroWhenAllSideWhenCatagoryIsNotSpecified() {
        Yacht game = new Yacht("12335", Fours);
        assertEquals(0, game.result());

    }

    @Test
    void expectSumOfPipsWhenChoiceCatagoryIsGiven() {
        Yacht game = new Yacht("23346", Choice);
        assertEquals(18, game.result());

    }
    @Test
    void expect30PointsForAPerticularCombination() {
        Yacht game = new Yacht("12345", LittleStraight);
        assertEquals(30, game.result());

    }
    @Test
    void expect30PointsForAnotherPerticularCombination() {
        Yacht game = new Yacht("23456", BigStraight);
        assertEquals(30, game.result());

    }

    @Test
    void expect0PointsWhenWrongCatagoryIsGiven() {
        Yacht game = new Yacht("56453", BigStraight);
        assertEquals(0, game.result());

    }

    @Test
    void expect0PointsWhenNumberOfThrowsAreGreaterThanFive() {
        Yacht game = new Yacht("564537578", BigStraight);
        assertEquals(0, game.result());

    }

    @Test
    void expect50PointsWhenCatagoryIsYatch() {
        Yacht game = new Yacht("55555", Yatch);
        assertEquals(50, game.result());

    }

    @Test
    void expect0PointsWhenCatagoryIsYatchButThePatternDoesNotFallInTheCatagory() {
        Yacht game = new Yacht("55554", Yatch);
        assertEquals(0, game.result());

    }

    @Test
    void expect16PointsWhenCatagoryIsFourOfAKind() {
        Yacht game = new Yacht("44445", FOUR_OF_A_KIND);
        assertEquals(16, game.result());

    }

    @Test
    void expect0PointsWhenCatagoryIsFourOfAKindButPatternDoesNotMatch() {
        Yacht game = new Yacht("44455", FOUR_OF_A_KIND);
        assertEquals(0, game.result());

    }

    @Test
    void expect19PointsWhenCatagoryIsFullHouse() {
        Yacht game = new Yacht("24224", FULL_HOUSE);
        assertEquals(14, game.result());

    }

}
